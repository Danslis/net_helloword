﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace net_helloword.Models
{
    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get;set; }
    }
}
